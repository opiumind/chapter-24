package problem14;

/**
 * (Prime number iterator) Define an iterator class named PrimeIterator for iter-
 ating prime numbers. The constructor takes an argument that specifies the limit
 of the maximum prime number. For example, new PrimeIterator(23302)
 creates an iterator that iterates prime numbers less than or equal to 23302 . Write
 a test program that uses this iterator to display all prime numbers less than or
 equal to 100000 .
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        PrimeIterator myPrimeIterator = new PrimeIterator(97L);
        while (myPrimeIterator.hasNext()) {
            System.out.print(myPrimeIterator.next() + " ");
        }
    }
}
