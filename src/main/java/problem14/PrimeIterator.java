package problem14;

import java.util.ArrayList;
import java.util.Iterator;

public class PrimeIterator implements Iterator<Long> {
    private ArrayList<Long> primeNumbers = new ArrayList<>();
    private int currentIndex = 0;

    PrimeIterator(Long limit) {
        this.primeNumbers = setPrimeNumbers(limit);
    }

    ArrayList<Long> setPrimeNumbers(Long limit) {
        ArrayList<Long> primeNumbers = new ArrayList<>();
        primeNumbers.add(2L);
        for (Long i = 2L; i <= limit; i++) {
            for (int j = 0; j < primeNumbers.size(); j++) {
                if (i % primeNumbers.get(j) == 0) {
                    break;
                }
                if (j == primeNumbers.size() - 1) {
                    primeNumbers.add(i);
                }
            }
        }
        primeNumbers.add(0,1L);
        return primeNumbers;
    }

    @Override
    public boolean hasNext() {
        return (currentIndex < primeNumbers.size());
    }

    @Override
    public Long next() {
        if (hasNext()) {
            currentIndex++;
            return primeNumbers.get(currentIndex - 1);
        }
        return null;
    }
}
