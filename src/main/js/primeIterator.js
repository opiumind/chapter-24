'use strict';
class PrimeIterator {
  constructor(limit) {
    this.primeNumbers = this.setPrimeNumbers(limit);
    this.currentIndex = 0;
  }

  setPrimeNumbers(limit) {
    let primeNumbers = [2];
    for (let i = 2; i <= limit; i++) {
      for (let j = 0; j < primeNumbers.length; j++) {
        if (i % primeNumbers[j] === 0) {
          break;
        }
        if (j === primeNumbers.length - 1) {
          primeNumbers.push(i);
        }
      }
    }
    primeNumbers.splice(0,0,1);
    return primeNumbers;
  }

  hasNext() {
    return (this.currentIndex < this.primeNumbers.length);
  }

  getNext() {
    if (this.hasNext()) {
      this.currentIndex++;
      return this.primeNumbers[this.currentIndex - 1];
    }
    return null;
  }
}

// function main() {
  let myPrimeIterator = new PrimeIterator(100000);
  while (myPrimeIterator.hasNext()) {
    console.log(myPrimeIterator.getNext());
  }
// }

// main();
module.exports = {
  myPrimeIterator: new PrimeIterator(1)
};