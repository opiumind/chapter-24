let assert = require('assert');
let myModule = require('../primeIterator');

describe('Prime iterator', function() {
  it('1. Get prime numbers.', function () {
    assert.deepEqual([1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79], myModule.myPrimeIterator.setPrimeNumbers(79));
  });

  it('2. Check if there is next number in an iterator.', function () {
    assert.equal(true, myModule.myPrimeIterator.hasNext());
  });

  it('3. Get next number in an iterator.', function () {
    assert.equal(1, myModule.myPrimeIterator.getNext());
  });
});