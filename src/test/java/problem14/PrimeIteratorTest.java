package problem14;

import junit.framework.TestCase;

/**
 * Created by Olga Lisovskaya on 10/15/17.
 */
public class PrimeIteratorTest extends TestCase {
    PrimeIterator myPrimeIterator = new PrimeIterator(1L);

    public void testSetPrimeNumbers() throws Exception {
        assertEquals("Wrong prime numbers.",
                "[1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79]",
                myPrimeIterator.setPrimeNumbers(79L).toString());
    }

    public void testHasNext() throws Exception {
        assertEquals("Wrong hasNext() value.", true, myPrimeIterator.hasNext());
    }

    public void testNext() throws Exception {
        assertEquals("Wrong next prime number.", Long.valueOf(1L), myPrimeIterator.next());
    }

}